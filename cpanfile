#!/usr/bin/perl
requires 'URI';
requires 'Dancer';
requires 'File::Assets';
requires 'Dancer::Plugin';
requires 'CSS::Minifier';
requires 'CSS::Minifier::XS';
requires 'JavaScript::Minifier';
requires 'JavaScript::Minifier::XS';
on test => sub {
    requires "Test::More";
    requires "Test::Cucumber::Tiny";
};
